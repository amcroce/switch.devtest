﻿namespace SharpBank.Enums
{
    public enum AccountType
    {
        CHECKING,
        SAVINGS,
        MAXI_SAVINGS
    }
}
