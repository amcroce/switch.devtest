﻿using System;
using System.Collections.Generic;
using SharpBank.Enums;

namespace SharpBank
{
    public class Account
    {
        private readonly AccountType accountType;
        public List<Transaction> transactions;

        public Account(AccountType accountType)
        {
            this.accountType = accountType;
            this.transactions = new List<Transaction>();
        }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }

            transactions.Add(new Transaction(amount));
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }

            transactions.Add(new Transaction(-amount));
        }

        public double InterestEarned()
        {
            double amount = SumTransactions();
            switch (accountType)
            {
                case AccountType.SAVINGS:
                    if (amount <= 1000)
                        return amount * 0.001;
                    else
                        return 1 + (amount - 1000) * 0.002;
                case AccountType.MAXI_SAVINGS:
                    if (transactions.Exists(t => t.amount < 0 && (DateTime.UtcNow.Date - t.transactionDate.Date).TotalDays <= 10))
                        return amount * 0.001;
                    return amount * 0.05;
                default:
                    return amount * 0.001;
            }
        }

        public double SumTransactions()
        {
            double amount = 0.0;
            foreach (Transaction t in transactions)
                amount += t.amount;
            return amount;
        }

        public AccountType GetAccountType()
        {
            return accountType;
        }
    }
}
