﻿using System;
using System.Collections.Generic;
using SharpBank.Enums;

namespace SharpBank
{
    public class Customer
    {
        private String name;
        private List<Account> accounts;

        public Customer(String name)
        {
            this.name = name;
            this.accounts = new List<Account>();
        }

        public String GetName()
        {
            return name;
        }

        public Customer OpenAccount(Account account)
        {
            // I will asume the customer can only have one account of each type
            if (accounts.Exists(a => a.GetAccountType() == account.GetAccountType()))
            {
                throw new ArgumentException($"An account of type {account.GetAccountType()} already exists");
            }
            accounts.Add(account);
            return this;
        }

        public int GetNumberOfAccounts()
        {
            return accounts.Count;
        }

        public double TotalInterestEarned()
        {
            double total = 0;
            foreach (Account a in accounts)
                total += a.InterestEarned();
            return total;
        }

        public void Transfer(AccountType fromAccountType, AccountType toAccountType, double amount)
        {
            var from = GetAccountByType(fromAccountType);
            
            from.Withdraw(amount);

            try
            {
                GetAccountByType(toAccountType).Deposit(amount);
            }
            catch (Exception ex)
            {
                // If transaction fails, return the money to the origin account
                from.Deposit(amount);
                throw ex;
            }
        }

        /*******************************
         * This method gets a statement
         *********************************/
        public String GetStatement()
        {
            //JIRA-123 Change by Joe Bloggs 29/7/1988 start
            String statement = null; //reset statement to null here
            //JIRA-123 Change by Joe Bloggs 29/7/1988 end
            statement = "Statement for " + name + "\n";
            double total = 0.0;
            foreach (Account a in accounts)
            {
                statement += "\n" + StatementForAccount(a) + "\n";
                total += a.SumTransactions();
            }
            statement += "\nTotal In All Accounts " + ToDollars(total);
            return statement;
        }

        private Account GetAccountByType(AccountType type)
        {
            var account = accounts.Find(a => a.GetAccountType() == type);

            if (account == null)
            {
                throw new ArgumentException($"No account of type {type} exists for customer {name}.");
            }

            return account;
        }

        private String StatementForAccount(Account a)
        {
            String s = "";

            //Translate to pretty account type
            switch (a.GetAccountType())
            {
                case AccountType.CHECKING:
                    s += "Checking Account\n";
                    break;
                case AccountType.SAVINGS:
                    s += "Savings Account\n";
                    break;
                case AccountType.MAXI_SAVINGS:
                    s += "Maxi Savings Account\n";
                    break;
            }

            //Now total up all the transactions
            double total = 0.0;
            foreach (Transaction t in a.transactions)
            {
                s += "  " + (t.amount < 0 ? "withdrawal" : "deposit") + " " + ToDollars(t.amount) + "\n";
                total += t.amount;
            }
            s += "Total " + ToDollars(total);
            return s;
        }

        private String ToDollars(double d)
        {
            return String.Format("${0:N2}", Math.Abs(d));
        }
    }
}
