﻿using NUnit.Framework;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {
        [Test]
        public void TestCustomerStatementGeneration()
        {

            Account checkingAccount = new Account(Enums.AccountType.CHECKING);
            Account savingsAccount = new Account(Enums.AccountType.SAVINGS);

            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);

            Assert.AreEqual("Statement for Henry\n" +
                    "\n" +
                    "Checking Account\n" +
                    "  deposit $100.00\n" +
                    "Total $100.00\n" +
                    "\n" +
                    "Savings Account\n" +
                    "  deposit $4,000.00\n" +
                    "  withdrawal $200.00\n" +
                    "Total $3,800.00\n" +
                    "\n" +
                    "Total In All Accounts $3,900.00", henry.GetStatement());
        }

        [Test]
        public void TestOneAccount()
        {
            Customer oscar = new Customer("Oscar").OpenAccount(new Account(Enums.AccountType.SAVINGS));
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Account(Enums.AccountType.SAVINGS));
            oscar.OpenAccount(new Account(Enums.AccountType.CHECKING));
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Account(Enums.AccountType.SAVINGS));
            oscar.OpenAccount(new Account(Enums.AccountType.CHECKING));
            oscar.OpenAccount(new Account(Enums.AccountType.MAXI_SAVINGS));
            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestOnlyOneAccountPerType()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Account(Enums.AccountType.SAVINGS));
            oscar.OpenAccount(new Account(Enums.AccountType.CHECKING));
            Assert.Throws<ArgumentException>(() => oscar.OpenAccount(new Account(Enums.AccountType.CHECKING)));
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTransferSuccess()
        {
            Account checkingAccount = new Account(Enums.AccountType.CHECKING);
            Account savingsAccount = new Account(Enums.AccountType.SAVINGS);

            Customer henry = new Customer("Henry")
                .OpenAccount(checkingAccount).OpenAccount(savingsAccount);

            checkingAccount.Deposit(100.0);
            savingsAccount.Deposit(4000.0);

            henry.Transfer(Enums.AccountType.SAVINGS, Enums.AccountType.CHECKING, 1000);

            Assert.AreEqual(checkingAccount.SumTransactions(), 1100.0);
            Assert.AreEqual(savingsAccount.SumTransactions(), 3000.0);
        }

        [Test]
        public void TestTransferFail()
        {
            Account checkingAccount = new Account(Enums.AccountType.CHECKING);

            Customer henry = new Customer("Henry")
                .OpenAccount(checkingAccount);

            checkingAccount.Deposit(1000.0);

            Assert.Throws<ArgumentException>(() => henry.Transfer(Enums.AccountType.CHECKING, Enums.AccountType.SAVINGS, 500.0));
            Assert.AreEqual(checkingAccount.SumTransactions(), 1000.0);
        }
    }
}
